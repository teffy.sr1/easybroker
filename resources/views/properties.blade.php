@extends('layout')
@section('content')
    <div class="row px-5 py-2 justify-content-center">
        <h6>Propiedades disponibles</h6>

        @if(!count($properties))
            <div class="text-center">
                <p class="fs-4"> <span class="text-danger">Opps!</span> No hay propiedades disponibles en esta página,</p>
                <p class="fs-4">por favor seleccione otra.</p>
            </div>
        @endif
        @foreach ($properties as $property)
            <div class="card m-3 col-lg-3 col-md-6 col-6 p-0" style="width: 18rem;">
                <img src="{{ $property->title_image_thumb }}"
                     onerror="this.onerror=null;
    this.src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVmOILdwqqtUCEeEqaQOK87BQO5iyvidbQhg&usqp=CAU'"
                     class="card-img-top" alt="{{ $property->title }}">
                <div class="card-body">
                    <h5 class="card-title">{{ $property->title }}</h5>
                    <p class="card-text">{{ $property->location }}</p>
                    <p class="card-text">{{ $property->property_type }} - <small
                            class="text-body-secondary">{{ $property->operations[0]->formatted_amount }}</small></p>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row justify-content-center">
        <div class="d-inline-block text-center">
            {{$properties->links('paginator')}}
        </div>
    </div>

@endsection
