@extends('layout')
@section('content')
<div class="d-flex align-items-center justify-content-center vh-100">
    <div class="text-center">
        <p class="fs-3"> <span class="text-danger">Opps!</span> An error has occurred, please try again later.</p>
    </div>
</div>
@endsection
