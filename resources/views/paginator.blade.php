@if($paginator->hasPages())
    <nav aria-label="{{ __('Pagination Navigation') }}" class="d-inline-block">
        <ul class="pagination">
            @if ($paginator->onFirstPage())
                <li class="page-item disabled">
                    <span class="page-link">{!! __('pagination.previous') !!}</span>
                </li>
            @else
                <li class="page-item">
                    <a href="{{ $paginator->previousPageUrl() }}"
                       class="page-link">{!! __('pagination.previous') !!}</a>
                </li>
            @endif

            @foreach ($elements as $element)
                @if (is_string($element))
                    <li class="page-item">
                            <span class="page-link">{{ $element }}</span>
                        </li>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        <li class="page-item @if ($page == $paginator->currentPage()) active @endif"
                            aria-current="page">
                            <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                        </li>
                    @endforeach
                @endif
            @endforeach

            @if ($paginator->hasMorePages())
                <li class="page-item">
                    <a href="{{ $paginator->nextPageUrl() }}" class="page-link"> {!! __('pagination.next') !!}</a>
                </li>
            @else
                <li class="page-item disabled">
                    <span class="page-link"> {!! __('pagination.next') !!}</span>
                </li>
            @endif
        </ul>
    </nav>
@endif
