<?php

namespace Tests\Unit\Helpers;

use App\Helpers\Paginated;
use PHPUnit\Framework\TestCase;

class PaginatedTest extends TestCase
{

    /**
     * @dataProvider additionProvider
     */
    public function test_paginatedItems(string $a, array $b, int $c, int $expected)
    {
        $result = Paginated::paginatedItems($a, $b, $c);
        $this->assertArrayHasKey('total', ['total' => $expected]);
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $result);

    }

    public function additionProvider(): array
    {
        return [
            'with 0 items'  => ['test', [], 0, 0],
            'with 3 items'  => ['test', [1,2,3], 10 , 3],

        ];
    }
}
