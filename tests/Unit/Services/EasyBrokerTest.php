<?php

namespace Tests\Unit\Services;

use App\Services\EasyBroker;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class EasyBrokerTest extends TestCase
{

    public function test_getProperties()
    {
        $expected = (object)['status' => 200];
        $contents = \Mockery::mock(StreamInterface::class);
        $response = \Mockery::mock(ResponseInterface::class);
        $response->shouldReceive('getBody')->once()->andReturn($contents);
        $contents->shouldReceive('getContents')->once()->andReturn(json_encode($expected));

        $client = \Mockery::mock(Client::class);
        $client->shouldReceive('request')->once()->andReturn($response);

        $easyBroker = new EasyBroker($client);
        $data = $easyBroker->getProperties();
        $this->assertEquals($expected, $data);

    }


    public function test_getPropertiesExeptions()
    {
        $msgExpected = 'An error has occurred, please try again later.';
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage($msgExpected);

        $exception = new \Exception($msgExpected);
        $client = \Mockery::mock(Client::class);
        $client->shouldReceive('request')->once()->andThrow($exception);

        $easyBroker = new EasyBroker($client);
        $data = $easyBroker->getProperties();
    }

}
