<?php

namespace App\Http\Controllers;

use App\Helpers\Paginated;
use App\Http\Requests\PropertiesListRequest;
use App\Services\EasyBroker;

class PropertiesController extends Controller
{
    /**
     * @var Paginated
     */
    private $paginatedHelper;
    /**
     * @var EasyBroker
     */
    private $easybrokerService;

    public function __construct(Paginated $paginatedHelper, EasyBroker $easyBrokerService)
    {
        $this->paginatedHelper = $paginatedHelper;
        $this->easybrokerService = $easyBrokerService;
    }

    public function list(PropertiesListRequest $request)
    {
        try {
            $page = $request->get('page') ?? 1;

            $properties = $this->easybrokerService->getProperties($page);

            $paginatedItems = $this->paginatedHelper->paginatedItems(
                $request->url(),
                $properties->content,
                $properties->pagination->total,
                $properties->pagination->limit
            );

            return response()->view('properties', ['properties' => $paginatedItems]);
        } catch (\Exception $e) {
            return response()->view('error');
        }

    }
}
