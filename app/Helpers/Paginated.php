<?php

namespace App\Helpers;

use Illuminate\Pagination\LengthAwarePaginator;

class Paginated
{
 public function paginatedItems($url, $currentPageItems, $totalItems, $perPage = 10)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($currentPageItems);
        $paginatedItems= new LengthAwarePaginator($itemCollection , $totalItems, $perPage, $currentPage);
        $paginatedItems->setPath($url);

        return $paginatedItems;
    }
}
