<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class EasyBroker
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client ){
        $this->client = $client;
    }

    /**
     * @param int $page
     * @return mixed
     * @throws GuzzleException
     */
    public function getProperties(int $page = 1, int $limit = 12)
    {
        try {
            $response = $this->client->request(
                'GET',
                env('API_EASYBROKER') . 'properties?limit=' . $limit . '&page=' . $page,
                [
                    'headers' => [
                        'X-Authorization' => env('API_EASYBROKER_KEY'),
                        'accept' => 'application/json',
                    ],
                ]
            );

            return json_decode($response->getBody()->getContents());

        } catch (GuzzleException $e) {
            Log::error($e->getMessage());
            throw new \Exception('An error has occurred, please try again later.');
        }

    }
}
